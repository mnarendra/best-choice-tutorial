    $(document).ready(function() {

        // $(function(){
        //    $.get("header.html", function(data) {
        //        $("#header").html(data);
        //   });
        // });

        // Jquery for scrolling menu bar 
        $(window).on('scroll', function(event) {
            var scrollValue = $(window).scrollTop();
            if (scrollValue > 70 || scrollValue > 20) {
                $('.navbar').addClass('affix');
                $('.get-start').addClass('affix');
            } else {
                $('.navbar').removeClass('affix');
                $('.get-start').removeClass('affix');
            }
        });

        //navigation active starts here
        $('.navbar-nav a').click(function(e) {
            // e.preventDefault();
            $('a').removeClass('active');
            $(this).addClass('active');
        });


       //getting data from server latest news
        $.getJSON('http://localhost:3000/latest_news', function(data) {
            console.log(data);
            for (var i = 0; i <= data.length; i++) {
                var myCol = $('<div class="col-sm-6 col-md-6 pb-2"></div>');
                // var myPanel = $('<div class="card card-outline-info" id="' + i + 'Panel"><div class="card-block"><div class="card-title"><span>Card #' + i + '</span><button type="button" class="close" data-target="#' + i + 'Panel" data-dismiss="alert"><span class="float-right"><i class="fa fa-remove"></i></span></button></div><p>Some text in ' + i + '</p><img src="https://cdn-images-1.medium.com/max/400/1*Xd9PslNemQpco79bpCaWsQ.jpeg"></div></div>');
                var myPanel =$('<div class="card-main">'
                        +  '<div class="row">'
                        +       '<div class="col-lg-5 col-sm-5 col-5 pr-0">'
                        +            '<div class="c-img">'
                        +                '<img src="https://cdn-images-1.medium.com/max/400/1*Xd9PslNemQpco79bpCaWsQ.jpeg">'
                        +           '</div>'
                        +       '</div>'
                        +       '<div class="col-lg-7 col-sm-7 col-7 pl-0">'
                        +            '<div class="c-data">'
                        +                '<div class="mem-blog-info">'
                        +                    '<h5 class="c-data-head">'+data[i].topic+'</h5>'
                        +                    '<p class="c-data-dec"><a href="story.html">'+data[i].t_dec+'...</a></p>'
                        +                '</div>'
                        +                '<div class="mem-info">'
                        +                    '<div class="media">'
                        +                        '<div class="up-img">'
                        +                           '<img class="mr-2 rounded-circle" src='+data[i].timg+' alt="Generic placeholder image">'
                        +                        '</div>'
                        +                        '<div class="media-body">'
                        +                            '<h5 class="mt-0">Cassius Kiani</h5>'
                        +                            '<span class="mdate-o">Nov 22: 11 min read</span>'
                        +                            '<div class="mem-bm"><i class="zmdi zmdi-bookmark-outline"></i></div>'
                        +                       '</div>'
                        +                    '</div>'
                        +               '</div>'
                        +          '</div>'
                        +        '</div>'
                        +  '</div>'
                        + '</div>');

                myPanel.appendTo(myCol);
                myCol.appendTo('#contentPanel');
            }

        });
 
    //getting data from server latest news
        // $.getJSON('http://localhost:3000/latest_news', function(data) {

        //         data.forEach( function(obj) {
        //           var img = new Image();
        //           img.src = obj.timg;
        //           img.setAttribute("class", "banner-img");
        //           img.setAttribute("alt", "effy");
        //           document.getElementById("profile-img").appendChild(img);
        //         });

        // });
 
       //getting data from server for latest news


      //signUp user
      $("#signUpForm").submit(function(event) {

        // Stop form from submitting normally
        event.preventDefault();

        // Get some values from elements on the page:
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var cpassword = $("#cpassword").val();

        var data = {
            "firstName": fname,
            "lastLame": lname,
            "email": email,
            "password": password,
            "confirmPassword": cpassword
        }
        var url = "http://localhost:3000/signUp"
        // Send the data using post
        var posting = $.post(url, data);

        // Put the results in a div
        posting.done(function(data) {
            // $("#sdata").text("created employee is :" + data.first_name);
            alert("user created successfully!");
            location.assign("login.html");
        });
    });
       //signUp user ends

       //login user
      $("#loginForm").submit(function(event) {

        // Stop form from submitting normally
        event.preventDefault();

        // Get some values from elements on the page:
        var email = $("#email").val();
        var password = $("#password").val();


        var data = {
            "email": email,
            "password": password,
        }
        var url = "http://localhost:3000/login"
        // Send the data using post
        var postData = $.post(url, data);

        // Put the results in a div
        postData.done(function(data) {
            location.assign("index.html");
        });
    });
       //login user ends

      //log user
      $("#forgotForm").submit(function(event) {

        // Stop form from submitting normally
        event.preventDefault();

        // Get some values from elements on the page:
        var email = $("#email").val();

        var data = {
            "email": email
        }
        var url = "http://localhost:3000/forgot"
        // Send the data using post
        var postData = $.post(url, data);

        // Put the results in a div
        postData.done(function(data) {
            alert("Please verify your email for reset password")
            location.assign("login.html");
        });
    });
       //login user ends


    });